$(document).ready(function () {

	// SLIDER https://github.com/ganlanyuan/tiny-slider
	var slider = tns({
		container: '.trust__slider',
		autoplayTimeout: 3000,
		items: 2,
		controls: false,
		nav: false,
		autoplay: true,
		autoplayButtonOutput: false,
		responsive: {
			992: {
				items: 8
			},
			768: {
				items: 6
			},
			576: {
				items: 4
			}
		}
	});


	// VIDEO
	var video = $("#video").attr("src");

	$('#play-video').on('click', function (ev) {
		$("#video").attr("src", "https://www.youtube.com/embed/PRqD6Ohh9g8?autoplay=1");
		ev.preventDefault();
	});

	$('#videoModal').on('click', function (ev) {
		$("#video").attr("src", "");
		$("#video").attr("src", video);
		$("#video")[0].src = "";
		ev.preventDefault();
	});


	// HOW IT WORKS 
	howImgs = [];
	for (var i = 0; i < 5; i++) {
		howImgs[i] = i;
	};

	$.each(howImgs, function (i) {
		$(".how__list li:nth-child(" + (i + 1) + ")").mouseover(function () {
			$(".how__img").css("background-image", "url('assets/imgs/how_" + (i + 1) + ".png')");
		});
	});


	// CERTIFICAT ANIMATION
	if ($(".certificat")[0]) {
		$(window).scroll(function () {
			var hT = $('.certificat').offset().top,
				hH = $('.certificat').outerHeight(),
				wH = $(window).height(),
				wS = $(this).scrollTop();
			if (wS > (hT + hH - wH)) {
				$(".certificat__img").addClass("certificat__img--show");
			}
		});
	};
});