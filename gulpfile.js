var gulp = require('gulp');
var browserSync = require('browser-sync');

// Init browserSync
gulp.task('browserSync', function() {
	browserSync({
		server: {
			baseDir: 'app'
		},
	})
})

//Watch task
gulp.task('default', ['browserSync'], function() {
  gulp.watch('app/**/*', browserSync.reload); 
});